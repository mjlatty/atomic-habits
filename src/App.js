import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          HABITS X TIME = YOU
        </p>
        <HabitList/>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Atomic Habits
        </a>
      </header>
    </div>
  );
}

function HabitList() {
  return(
    <div>
      <ul>
        <li>Mind</li>
        <li>Body</li>
        <li>Relationships</li>
        <li>Work</li>
        <li>Home</li> 
        <li>Hobbies</li>
        <li>Finances</li>
        <li>Volunteering</li>
      </ul>
    </div>
  )
}

export default App;
